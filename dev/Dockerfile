# Distributed under the terms of the MIT License
# This file and the associated scripts have been adapted from
# the Jupyter Notebook docker files available at https://github.com/jupyter/docker-stacks, and licensed under the Modified BSD License.

From bempp/base:latest

Maintainer Timo Betcke <timo.betcke@gmail.com>

USER root

RUN apt-get update && apt-get -y install \
    dh-make \
    gcc \
    g++ \
    gfortran \
    libeigen3-dev \
    python-dev \
    python3-dev \
    python-numpy \
    python3-numpy \
    patchelf \
    libtbb-dev \
    zlib1g-dev \
    libboost-all-dev \
    cmake \
    cpio \
    libdune-common-dev \
    libdune-geometry-dev \
    libdune-grid-dev \
    libdune-localfunctions-dev \
    mpi-default-dev \
    cython \
    cython3 \
    python-setuptools \
    python3-setuptools \
    python-mpi4py \
    python3-mpi4py \
    python-scipy \
    python3-scipy \
    dh-python \
    python-all \
    python3-all \
    libpython3.5 \
    libpython2.7 \
    libtbb2 \
    libstdc++6 \
    vim \
    gmsh \
    git \
    && apt-get clean

ENV NB_USER bempp
ENV NB_UID 1000
ENV SHELL /bin/bash
ENV HOME /home/$NB_USER
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV PYTHONPATH /home/$NB_USER/python

RUN mkdir /home/$NB_USER/bempp && \
    mkdir /home/$NB_USER/python && \
    echo "cacert=/etc/ssl/certs/ca-certificates.crt" > /home/$NB_USER/.curlrc

USER root
EXPOSE 8888
WORKDIR /home/$NB_USER/bempp
CMD ["/bin/bash"]

RUN echo '#!/bin/bash\nset -e\njupyter notebook $*\n' >> /usr/local/bin/start-notebook.sh && chmod 755 /usr/local/bin/start-notebook.sh

RUN echo "c = get_config()\nc.NotebookApp.ip = '*'\nc.NotebookApp.port = 8888\nc.NotebookApp.open_browser = False\n" >> /home/$NB_USER/.jupyter/jupyter_notebook_config.py && chown -R $NB_USER:users /home/$NB_USER/.jupyter

USER $NB_USER
